# Engine   
This repository contains every file associated with the engine.  
The first static test is planned for late November 2020.   
A 800 N thrust is expected for a burn time of approximately 14 seconds.  
## Development  
The CAD was developed with Autodesk Fusion 360.  
All the bolts,washers,nuts and other various off-the-self components follow the ISO metric standards.   
## Materials  
Aluminum 7075-T6 is used for the lathe and mill-cut parts,6082-T6 for the combustion chamber and pre/post combustor,   
steel for the standard components and graphite for the nozzle.    
## License
This project is licensed under the [CERN OHLv1.2](https://gitlab.com/white-noise/cronos-rocket/propulsion/-/blob/master/LICENSE)  
